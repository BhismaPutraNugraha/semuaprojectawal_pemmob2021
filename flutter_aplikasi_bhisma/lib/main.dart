import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.greenAccent,
          title: Text('Aplikasi IMBPN STORE'),
          leading: Icon(
            Icons.dashboard_outlined,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.thumb_up),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.thumb_down),
              onPressed: () {},
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: [
              Image(
                image: NetworkImage(
                    'https://static1.undiksha.ac.id/siakng/2021/fotomahasiswa/421178189-1610531134.png'),
              ),
              Padding(
                padding: EdgeInsets.all(18.0),
                child: Text(
                  'CEO : I Made Bhisma Putra Nugraha',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
